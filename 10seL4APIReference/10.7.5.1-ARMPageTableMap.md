### 10.7.5  seL4_ARM_PageTable

#### 10.7.5.1  Map

static inline int seL4_ARM_PageTable_Map

映射一个页表。

类型 | 名字 | 描述
--- | --- | ---
seL4_ARM_PageTable | _service | 要操作的页表能力句柄。自当前线程根CNode按机器字位数解析，下同
seL4_CPtr | vspace | 要映射到的VSapce一级页表能力句柄，必须已经在ASID池中赋值
seL4_Word | vaddr | 要映射到的虚拟地址
seL4_ARM_VMAttributes | attr | 虚拟内存页属性。第7章给出了可能的值

*返回值*：返回0表示成功，非0值表示有错误发生。第10.1节描述了发生错误时消息寄存器和消息标签有关内容。

*描述*：给定一个VSpace能力，按指定虚拟地址为请求的PageTable建立一个映射项。如果指定虚拟地址的上层页表结构不存在，则操作失败并返回seL4_FailedLookup错误；如果该页表已经映射，则操作失败并返回seL4_InvalidCapability错误；如果有其它对象已经映射到该表项，则操作失败并返回seL4_DeleteFirst错误。
